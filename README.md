Custom Dockerfile

# Commandes

## Build 
```bash
docker build --tag kyoden/php:7.1-fpm -f 7.1-fpm . 
```

## Pusher sur le hub docker
```bash
docker login
docker image push kyoden/php:7.1-fpm
```

## Purge
Supprimer toutes images `<none>` (créées lors de builds infructueux ou autre) : 
```bash
docker rmi $(docker images | grep "<none>" | awk '{print $3}')
```

Voir l'article suivante http://tech.sensiolabs.com/2017/01/26/docker-cleanup.html
sur diverses commandes pour purger les donners.