Inspired of Docker prooph/php

# Environment:
- `PHP_TIMEZONE=Europe/Paris` (default: UTC)
- `PHP_ENABLE_XDEBUG=yes|no` (default: no)
- `PHP_ENABLE_BLACKFIRE=yes|no` (default: no)
- `PHP_ENABLE_OPCACHE=yes|no` (default: no)
- `PHP_ENABLE_MONGODB=yes|no` (default: no)

# PHP
## Version
```bash
❯ docker run --rm -it kyoden/php:7.1-fpm php -v                                                                                                                                                                             [12:09:38]
PHP 7.1.3 (cli) (built: Mar 17 2017 22:28:12) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.1.0, Copyright (c) 1998-2017 Zend Technologies
    with Zend OPcache v7.1.3, Copyright (c) 1999-2017, by Zend Technologies
    
❯ docker run --rm -it -e PHP_ENABLE_XDEBUG=yes kyoden/php:7.1-fpm php -v                                                                                                                                                    [12:10:32]
PHP 7.1.3 (cli) (built: Mar 17 2017 22:28:12) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.1.0, Copyright (c) 1998-2017 Zend Technologies
    with Zend OPcache v7.1.3, Copyright (c) 1999-2017, by Zend Technologies
    with Xdebug v2.6.0-dev, Copyright (c) 2002-2017, by Derick Rethans
```
## Modules
```bash
❯ docker run --rm -it  kyoden/php:7.1-fpm php -m                                                                                                                                                                            [12:11:55]
[PHP Modules]
amqp
bcmath
Core
ctype
curl
date
dom
fileinfo
filter
ftp
hash
iconv
intl
json
libsodium
libxml
mbstring
mcrypt
mysqlnd
openssl
pcntl
pcre
PDO
pdo_mysql
pdo_pgsql
pdo_sqlite
Phar
posix
readline
redis
Reflection
session
SimpleXML
SPL
sqlite3
standard
tokenizer
xml
xmlreader
xmlwriter
Zend OPcache
zip
zlib
zmq

[Zend Modules]
Zend OPcache
```

# miscellaneous
## Composer
Composer is installed