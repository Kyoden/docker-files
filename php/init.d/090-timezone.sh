#!/bin/bash


: ${PHP_TIMEZONE:=UTC}

echo -e "date.timezone = $PHP_TIMEZONE" > /usr/local/etc/php/conf.d/timezone.ini
