#!/bin/bash

: ${PHP_ENABLE_BLACKFIRE:=no}

if [ "$PHP_ENABLE_BLACKFIRE" = "yes" ]; then
    cp /conf/blackfire.ini /usr/local/etc/php/conf.d/blackfire.ini
else 
    rm -f /usr/local/etc/php/conf.d/blackfire.ini
fi
