#!/bin/bash

: ${PHP_ENABLE_MONGODB:=no}

if [ "$PHP_ENABLE_MONGODB" = "yes" ]; then
    cp /conf/mongo.ini /usr/local/etc/php/conf.d/mongo.ini
    cp /conf/mongodb.ini /usr/local/etc/php/conf.d/mongodb.ini
else
    rm -f /usr/local/etc/php/conf.d/mongo.ini /usr/local/etc/php/conf.d/mongodb.ini
fi
