#!/bin/bash

: ${PHP_ENABLE_OPCACHE:=no}

if [ "$PHP_ENABLE_OPCACHE" = "yes" ]; then
    cp /conf/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
else 
    rm -f /usr/local/etc/php/conf.d/opcache.ini
fi
