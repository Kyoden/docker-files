#!/bin/bash

: ${PHP_ENABLE_XDEBUG:=no}

if [ "$PHP_ENABLE_XDEBUG" = "yes" ]; then
    cp /conf/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
else 
    rm -f /usr/local/etc/php/conf.d/xdebug.ini
fi
